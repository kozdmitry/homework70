import axios from 'axios';

export const FETCH_MENU_REQUEST = 'FETCH_MENU_REQUEST';
export const FETCH_MENU_SUCCESS = 'FETCH_MENU_SUCCESS';
export const FETCH_MENU_FAILURE = 'FETCH_MENU_FAILURE';

export const fetchMenuRequest = () => ({type: FETCH_MENU_REQUEST});
export const fetchMenuSuccess = menu => ({type: FETCH_MENU_SUCCESS, menu});
export const fetchMenuFailure = () => ({type: FETCH_MENU_FAILURE});

export const fetchMenu = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchMenuRequest());
            const response = await axios.get('https://cafe-homework70-default-rtdb.firebaseio.com/menu.json');
            dispatch (fetchMenuSuccess(response.data));
            console.log(response.data);
        } catch (error) {
            dispatch (fetchMenuFailure(error));
        }
    };
};