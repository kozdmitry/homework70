import {FETCH_MENU_REQUEST, FETCH_MENU_SUCCESS, FETCH_MENU_FAILURE} from "../../store/action/actionsMenu";

const initialState = {
    loading: false,
    error: false,
    menu: [],
};

const reducerMenu = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MENU_REQUEST:
            return {...state, loading: true, error: false};
        case FETCH_MENU_SUCCESS:
            return {...state, loading: false, menu: action.menu};
        case FETCH_MENU_FAILURE:
            return {...state, loading: false, error: true};
        default:
            return state;
    }
};

export default reducerMenu;