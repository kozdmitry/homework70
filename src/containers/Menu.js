import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchMenu} from "../store/action/actionsMenu";
import "./Menu.css";

const Menu = () => {
    const dispatch = useDispatch();
    const state = useSelector(state => state.menu);

    console.log(state);

    useEffect(() => {
        dispatch(fetchMenu())
    }, [dispatch]);

    return (
        <div className="menu">
                {state && Object.keys(state).map((key, index) => {
                    return (
                        <div className="text" index={index}>
                            <img className="img" src={state[key].image}/>
                            <h5>{state[key].name}</h5>
                            <p>Price: {state[key].price}</p>
                            <button>Add to cart =></button>
                        </div>
                    )
                })}
        </div>
    );
};

export default Menu;